# frozen_string_literal: true

require 'optparse'

module YT
  class Options
    def initialize(args)
      @options = {
        jobs: 5,
        out_dir: File.expand_path('.'),
      }

      args << '--help' if args.empty?
      parser.parse! args
    end

    def jobs
      @options[:jobs]
    end

    def out_dir
      @options[:out_dir]
    end

    def parser
      OptionParser.new do |opts|
        name = File.basename($PROGRAM_NAME)

        opts.banner = "Usage:\t#{name} [options] URL..."

        opts.on('-j JOBS', '--jobs JOBS', Integer,
                "Number of separate jobs to run, default is #{@options[:jobs]}") do |jobs|
          @options[:jobs] = jobs
        end

        opts.on('-D DIR', '--output-directory DIR',
                "directory to store files in, default is #{@options[:out_dir]}") do |dir|
          @options[:out_dir] = dir.tap do |d|
            abort "ERROR: '#{d}' is not a valid directory" unless File.directory?(d)
          end
        end

        opts.on '-h', '--help', 'Display this message' do
          puts opts
          exit
        end
      end
    end
  end
end
