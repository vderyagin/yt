# frozen_string_literal: true

module YT
  class Downloader
    def initialize(opts)
      @opts = opts
    end

    def call(url)
      system YT::Executable.find,
             '--newline',
             '--output',
             File.join(@opts.out_dir, '%(title)s (%(upload_date)s).%(ext)s'),
             url
    end
  end
end
