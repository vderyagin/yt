# frozen_string_literal: true

require 'concurrent'

module YT
  class Runner
    def initialize(args)
      abort('No youtube-dl or yt-dlp executable found in $PATH') unless YT::Executable.find

      opts = Options.new(args)
      @raw_urls = args
      @pool = Concurrent::FixedThreadPool.new(opts.jobs)
      @downloader = Downloader.new(opts)
    end

    def download(url)
      @downloader.call url
    end

    def call
      @raw_urls.each do |raw_url|
        URL.new(raw_url).expand.each do |url|
          @pool.post do
            download url.url
          end
        end
      end

      @pool.shutdown
      @pool.wait_for_termination
    end
  end
end
