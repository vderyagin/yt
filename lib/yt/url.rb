# frozen_string_literal: true

module YT
  class URL
    attr_reader :url

    EXTRACT_VIDEO_ID_RE = /(?<=^\[youtube\] )[^:\s]+(?=: (Extracting video information|Downloading webpage)$)/

    def initialize(url)
      # Sometimes URLs start with "-", which confuses youtube-dl
      @url = url[/\Ahttp/] ? url : "https://youtube.com/watch?v=#{url}"
    end

    def youtube?
      @url[%r{/((w{3}|m)\.)?youtu(\.be|be\.com)/}]
    end

    def youtube_playlist?
      youtube? && @url[%r{/(watch|playlist)\?list=[[:alnum:]\-_]+\z}]
    end

    def youtube_channel?
      youtube? && @url[%r{/(channel|user)/[[:alnum:]\-_]+\z}]
    end

    def youtube_single_video?
      youtube? && @url[%r{/watch\?v=.+}]
    end

    def expand
      Enumerator.new do |yielder|
        if youtube_channel? || youtube_playlist?
          IO.popen %W[youtube-dl --simulate #{@url}] do |io|
            until io.eof?
              video_id = io.readline.chomp[EXTRACT_VIDEO_ID_RE]
              yielder << self.class.new(video_id) if video_id
            end
          end
        else
          yielder << self
        end
      end
    end
  end
end
