# frozen_string_literal: true

require 'mkmf'

module MakeMakefile::Logging
  @logfile = File::NULL         # don't write mkmf.log
  @quiet = true                 # don't write to stdout either
end

module YT
  module Executable
    def self.find
      %w[yt-dlp youtube-dl].find(&method(:find_executable))
    end
  end
end
