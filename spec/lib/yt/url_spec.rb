# frozen_string_literal: true

RSpec.describe YT::URL do
  context 'url types' do
    describe '#youtube?' do
      it 'recognizes plain youtube urls' do
        expect(YT::URL.new('https://youtube.com/foo')).to be_youtube
        expect(YT::URL.new('http://youtube.com/foo')).to be_youtube
        expect(YT::URL.new('https://www.youtube.com/foo')).to be_youtube
      end

      it 'recognizes m.youtube.com urls' do
        expect(YT::URL.new('http://m.youtube.com/foo')).to be_youtube
      end

      it 'recognizes youtu.be urls' do
        expect(YT::URL.new('https://youtu.be/foo')).to be_youtube
      end

      it 'recognizes non-youtube urls' do
        expect(YT::URL.new('https://vimeo.com/234')).not_to be_youtube
      end
    end

    describe '#youtube_playlist?' do
      it 'recognizes youtube playlists' do
        url = YT::URL.new('https://www.youtube.com/playlist?list=foo12')
        expect(url).to be_youtube_playlist
      end

      it 'rejects plain video links' do
        expect(YT::URL.new('https://youtube.com/foo')).not_to be_youtube_playlist
      end
    end

    describe '#youtube_channel?' do
      it 'recognizes youtube channels' do
        expect(YT::URL.new('http://youtube.com/user/foo')).to be_youtube_channel
        expect(YT::URL.new('http://youtube.com/channel/foo')).to be_youtube_channel
      end

      it 'rejects playlists' do
        playlist = YT::URL.new('https://www.youtube.com/playlist?list=FOOE')
        expect(playlist).not_to be_youtube_channel
      end

      it 'rejects single videos' do
        expect(YT::URL.new('https://www.youtube.com/watch?v=foo')).not_to be_youtube_channel
      end

      it 'rejects non-youtube links' do
        expect(YT::URL.new('https://vimeo.com/123')).not_to be_youtube_channel
      end
    end

    describe '#youtube_single_video?' do
      it 'recognizes single youtube video' do
        expect(YT::URL.new('https://www.youtube.com/watch?v=foo')).to be_youtube_single_video
      end

      it 'rejects playlists' do
        playlist = YT::URL.new('https://www.youtube.com/playlist?list=foo12')
        expect(playlist).not_to be_youtube_single_video
      end

      it 'rejects channels' do
        chan = YT::URL.new('http://youtube.com/channel/foo')
        expect(chan).not_to be_youtube_single_video
      end

      it 'rejects non-youtube links' do
        expect(YT::URL.new('https://vimeo.com/123')).not_to be_youtube_single_video
      end
    end
  end
end
